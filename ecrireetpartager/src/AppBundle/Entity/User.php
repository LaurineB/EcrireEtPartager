<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Nouvelle", mappedBy="auteur")
     */
    protected $nouvelles;

    /**
     * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="auteur")
     */
    protected $commentaires;

    /**
     * @var string

     * @ORM\Column(name="image_profile", type="string", length=255, nullable=true)
     */
    protected $imageProfile = null;

    /**
     * @var string
     *
     * @ORM\Column(name="saved_image_profile", type="string", length=255, nullable=true)
     */
    protected $savedImageProfile;
    /**
     * User constructor
     */

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @return mixed
     */
    public function getNouvelles()
    {
        return $this->nouvelles;
    }

    /**
     * @param mixed $nouvelles
     */
    public function setNouvelles($nouvelles)
    {
        $this->nouvelles = $nouvelles;
    }

    /**
     * @return mixed
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }

    /**
     * @param mixed $commentaires
     */
    public function setCommentaires($commentaires)
    {
        $this->commentaires = $commentaires;
    }

    /**
     * @return string
     */
    public function getImageProfile()
    {
        return $this->imageProfile;
    }

    /**
     * @param string $imageProfile
     */
    public function setImageProfile($imageProfile)
    {
        $this->imageProfile = $imageProfile;
    }

    /**
     * @return string
     */
    public function getSavedImageProfile()
    {
        return $this->savedImageProfile;
    }

    /**
     * @param string $savedImageProfile
     */
    public function setSavedImageProfile($savedImageProfile)
    {
        $this->savedImageProfile = $savedImageProfile;
    }

}