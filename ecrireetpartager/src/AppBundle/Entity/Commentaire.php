<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Commentaire
 *
 * @ORM\Table(name="commentaire")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="string", length=255)
     */
    private $contenu;

    /**
     * @var int
     *
     * @ORM\Column(name="note", type="integer")
     */
    private $note;
    /**
     * @var \Datetime
     *
     * @ORM\Column(name="published_at",type="datetime", nullable=false)
     */
    private $published_at;

    /**
     * @ORM\ManyToOne(targetEntity="Nouvelle", inversedBy="commentaires")
     * @ORM\JoinColumn(name="nouvelle_id",referencedColumnName="id",nullable=false)
     */
    private $nouvelle;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="commentaires")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",nullable=false)
     */
    protected $auteur;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPseudo()
    {
        return $this->pseudo;
    }

    /**
     * @param mixed $pseudo
     */
    public function setPseudo($pseudo)
    {
        $this->pseudo = $pseudo;
    }

    /**
     * @return int
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param int $note
     */
    public function setNote($note)
    {
        $this->note = $note;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Commentaire
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @return \Datetime
     */
    public function getpublished_At()
    {
        return $this->published_at;
    }

    /**
     * @param \Datetime $published_at
     */
    public function setPublished_At($published_at)
    {
        $this->published_at = $published_at;
    }

    /**
     * @return Nouvelle
     */
    public function getNouvelle()
    {
        return $this->nouvelle;
    }

    /**
     * @param Nouvelle $nouvelle
     */
    public function setNouvelle($nouvelle)
    {
        $this->nouvelle = $nouvelle;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * @param mixed $auteur
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;
    }

}

