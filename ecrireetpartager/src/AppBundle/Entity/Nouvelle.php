<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * Nouvelle
 *
 * @ORM\Table(name="nouvelle")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\NouvelleRepository")
 */
class Nouvelle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string

     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var string
     * @ORM\Column(name="extrait", type="text")
     */
    private $extrait;


    /**
     * @var \Datetime
     *
     * @ORM\Column(name="published_at",type="datetime", nullable=true)
     */
    private $published_at;

    /**
     * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="nouvelle")
     */
    private $commentaires;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="nouvelles")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id",nullable=false)
     */
    private $user;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @ORM\ManyToMany(targetEntity="Categorie", cascade={"persist"})
     */
    private $categories;

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Nouvelle
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * @param string $contenu
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;
    }

    /**
     * @return string
     */
    public function getExtrait()
    {
        return $this->extrait;
    }

    /**
     * @param string $extrait
     */
    public function setExtrait($extrait)
    {
        $this->extrait = $extrait;
    }


    /**
     * @return mixed
     */
    public function getPublished_At()
    {
        return $this->published_at;
    }

    /**
     * @param mixed $published_at
     */
    public function setPublished_At($published_at)
    {
        $this->published_at = $published_at;
    }

    /**
     * @return ArrayCollection
     */
    public function getCommentaires()
    {
        return $this->commentaires;
    }


}

