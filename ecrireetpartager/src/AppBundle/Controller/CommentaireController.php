<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commentaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
class CommentaireController extends Controller
{
    /**
     * @Route("/MesCommentaires", name="commentaires")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function commentList(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository(Commentaire::class);

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idUser = $user->getId();

        $dql = "SELECT i FROM AppBundle:Commentaire i WHERE i.auteur = $idUser";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $commentaires = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            50/*limit per page*/
        );
        // replace this example code with whatever you need
        return $this->render('commentaire/list.html.twig', [
            'commentaires' => $commentaires
        ]);
    }

    /**
     * @Route("/MesCommentairesReçus", name="commentReceive")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */

    public function commentReceive(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository(Commentaire::class);

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idUser = $user->getId();

        $dql = "SELECT c FROM AppBundle:Commentaire c JOIN AppBundle:Nouvelle n WHERE n.auteur = $idUser";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $commentaires = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            50/*limit per page*/
        );
        // replace this example code with whatever you need
        return $this->render('commentaire/list.html.twig', [
            'commentaires' => $commentaires
        ]);
    }
    /**
     * @Route("/commentaire/create", name="commentCreate")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function commentCreate(Request $request)
    {
        $commentaire = new Commentaire();
        $form = $this->createForm(CommentaireType::class, $commentaire);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire->setPublishedAt(null);
            $commentaire->setAuteur($this->container->get('security.token_storage')->getToken()->getUser());
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($commentaire); // persist is used when the object is nit yet in database
            $em->flush(); // execute query

            $flash = sprintf("La commentaire %s a bien été envoyée", $commentaire->getTitre());
            $this->addFlash('success', $flash);

            return $this->redirectToRoute('commentaires');
        }
        return $this->render("commentaire/create.html.twig", ['form' => $form->createView()]);
    }

    /**
     * @Route("/commentaire/{id}", name="commentDetail")
     */
    public function commentDetail(Commentaire $commentaire)
    {
        $commentaires = $commentaire->getCommentaires();

        return $this->render('commentaire/detail.html.twig', ['commentaire' => $commentaire, 'commentaires' => $commentaires]);
    }

    /**
     * @Route("/commentaire/edit/{id}", name="commentEdit")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function commentEdit(Commentaire $commentaire, Request $request)
    {
        if ($commentaire->getAuteur()->getId() == $this->container->get('security.token_storage')->getToken()->getAuteur()->getId()) {
            $form = $this->createForm(CommentaireType::class, $commentaire);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->get('doctrine.orm.entity_manager');
                $em->flush(); // execute query

                $flash = sprintf("L'article %s a bien été modifié", $commentaire->getTitre());
                $this->addFlash('success', $flash);

                return $this->redirectToRoute('commentaires');
            }

            return $this->render('commentaire/edit.html.twig', ['commentaire' => $commentaire, 'form' => $form->createView()]);
        } else {
            throw new NotFoundHttpException("Cette commentaire n'est pas à vous!");
        }
    }

    /**
     * @Route("/commentaire/delete/{id}", name="commentDelete")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function commentDelete(Commentaire $commentaire)
    {
        if ($commentaire->getAuteur()->getId() == $this->container->get('security.token_storage')->getToken()->getAuteur()->getId()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->remove($commentaire);
            $em->flush();
        } else {
            throw new NotFoundHttpException("Ce commentaire n'est pas à vous!");
        }
        return $this->redirectToRoute('nouvelleDetail',['id' => $commentaire->getNouvelle()->getId()]);
    }
}
