<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commentaire;
use AppBundle\Entity\Nouvelle;
use AppBundle\Form\CommentaireType;
use AppBundle\Form\NouvelleType;
use AppBundle\Repository\CommentaireRepository;
use DoctrineExtensions\Query\Mysql\Date;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class NouvelleController extends Controller
{
    /**
     * @Route("/nouvelle", name="nouvelles")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function postList(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository(Nouvelle::class);

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $idUser = $user->getId();

        $dql = "SELECT i FROM AppBundle:Nouvelle i WHERE i.user = $idUser";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $nouvelles = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            50/*limit per page*/
        );
        // replace this example code with whatever you need
        return $this->render('nouvelle/list.html.twig', [
            'nouvelles' => $nouvelles
        ]);
    }

    /**
     * @Route("/nouvelle/create", name="nouvelleCreate")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function postCreate(Request $request)
    {
        $nouvelle = new Nouvelle();
        $form = $this->createForm(NouvelleType::class, $nouvelle);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // On boucle sur les catégories pour les lier à la nouvelle
            foreach ($request->query->get('categories') as $category) {
                $nouvelle->addCategorie($category);
            }
            $nouvelle->setPublishedAt(null);
            $nouvelle->setUser($this->container->get('security.token_storage')->getToken()->getUser());
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($nouvelle); // persist is used when the object is nit yet in database
            $em->flush(); // execute query

            $flash = sprintf("La nouvelle %s a bien été envoyée", $nouvelle->getTitre());
            $this->addFlash('success', $flash);

            return $this->redirectToRoute('nouvelles');
        }
        return $this->render("nouvelle/create.html.twig", ['form' => $form->createView()]);
    }

    /**
     * @Route("/nouvelle/{id}", name="nouvelleDetail")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function postDetail(Nouvelle $nouvelle, Request $request)
    {
        $commentaires = $nouvelle->getCommentaires();
        //$avg = CommentaireRepository::class->getAverageOfPost($nouvelle->getId());

        $commentaire = new Commentaire();
        $commentForm = $this->createForm(CommentaireType::class,$commentaire);

        $commentForm->handleRequest($request);
        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $commentaire->setUser($this->container->get('security.token_storage')->getToken()->getUser());
            $commentaire->setNouvelle($nouvelle);
            $commentaire->setPublished_At(new \DateTime());
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($commentaire); // persist is used when the object is nit yet in database
            $em->flush(); // execute query

            $flash = sprintf("Le commentaire a bien été envoyée");
            $this->addFlash('success', $flash);

            $commentForm = $this->createForm(CommentaireType::class,$commentaire)->createView();
            return $this->render('nouvelle/detail.html.twig', ['nouvelle' => $nouvelle, 'commentaires' => $commentaires, 'form' => $commentForm]);
        }
        $commentForm = $this->createForm(CommentaireType::class,$commentaire)->createView();
        return $this->render('nouvelle/detail.html.twig', ['nouvelle' => $nouvelle, 'commentaires' => $commentaires, 'form' => $commentForm]);
    }

    /**
     * @Route("/nouvelle/edit/{id}", name="nouvelleEdit")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function postEdit(Nouvelle $nouvelle, Request $request)
    {
        if ($nouvelle->getUser()->getId() == $this->container->get('security.token_storage')->getToken()->getUser()->getId()) {
            $form = $this->createForm(NouvelleType::class, $nouvelle);

            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $em = $this->get('doctrine.orm.entity_manager');
                $em->flush(); // execute query

                // On boucle sur les catégories de la nouvelle pour les supprimer
                foreach ($nouvelle->getCategories() as $category) {
                    $nouvelle->removeCategory($category);
                }

                // On boucle sur les catégories pour les lier à la nouvelle
                foreach ($request->query->get('categories') as $category) {
                    $nouvelle->addCategorie($category);
                }

                $flash = sprintf("L'article %s a bien été modifié", $nouvelle->getTitre());
                $this->addFlash('success', $flash);

                return $this->redirectToRoute('nouvelles');
            }

            return $this->render('nouvelle/edit.html.twig', ['nouvelle' => $nouvelle, 'form' => $form->createView()]);
        } else {
            throw new NotFoundHttpException("Cette nouvelle n'est pas à vous!");
        }
    }

    /**
     * @Route("/nouvelle/publish/{id}", name="nouvellePublish")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function postPublish(Nouvelle $nouvelle)
    {
        if($nouvelle->getUser()->getId() == $this->container->get('security.token_storage')->getToken()->getUser()->getId()) {
            if ($nouvelle->getPublished_At() == null) {
                $nouvelle->setPublished_At(new \DateTime());
                $flash = sprintf("Nouvelle publiée");
                $this->addFlash('success', $flash);
            } else {
                $flash = sprintf("Nouvelle déjà publiée");
                $this->addFlash('error', $flash);
            }
            $em = $this->get('doctrine.orm.entity_manager');
            $em->flush(); // execute query
            return $this->redirectToRoute('nouvelleDetail', ['id' => $nouvelle->getId()]);
        }
    }


    /**
     * @Route("/nouvelle/delete/{id}", name="nouvelleDelete")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function postDelete(Nouvelle $nouvelle)
    {
        if ($nouvelle->getUser()->getId() == $this->container->get('security.token_storage')->getToken()->getUser()->getId()) {
        $em = $this->get('doctrine.orm.entity_manager');
            // On boucle sur les catégories de la nouvelle pour les supprimer
            foreach ($nouvelle->getCategories() as $category) {
                $nouvelle->removeCategory($category);
            }
        $em->remove($nouvelle);
        $em->flush();
        } else {
            throw new NotFoundHttpException("Cette nouvelle n'est pas à vous!");
        }
    }
}
