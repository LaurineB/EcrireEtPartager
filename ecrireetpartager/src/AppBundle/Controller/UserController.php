<?php
/**
 * Created by PhpStorm.
 * User: labai
 * Date: 11/10/2017
 * Time: 18:53
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Commentaire;
use AppBundle\Entity\User;
use AppBundle\Form\ImageProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UserController extends Controller
{
    /**
     * @Route("/monCompte", name="monCompte")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function monCompte(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository(User::class);

        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();
        $idUser = $currentUser->getId();

       $user = $repository->find($idUser);

        $form = $this->createForm(ImageProfileType::class, $user);
        $formView = $form->createView();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if($user->getSavedImageProfile() != null) {
                $oldFile = $user->getSavedImageProfile();
                unlink($oldFile);
                $user->setSavedImageProfile(null);
            }
            $file = $user->getImageProfile();
            // Generate a unique name for the file before saving it
            $fileName = md5(uniqid()).'.'.$file->guessExtension();

            $file->move(
                $this->getParameter('image_profile_directory'),
                $fileName
            );

            $user->setImageProfile($fileName);
            $user->setSavedImageProfile($fileName);



            return $this->render('user/monCompte.html.twig', [
                'user' => $user,
                'form' => $formView
            ]);
        }
        // replace this example code with whatever you need
        return $this->render('user/monCompte.html.twig', [
            'user' => $user,
            'form' => $formView
        ]);
    }

    /**
     * @Route("/supprimer", name="supprimerMonCompte")
     * @IsGranted("ROLE_USER", message="Partie réservée aux personnes identifiées")
     */
    public function supprimerMonCompte() {

        $currentUser = $this->container->get('security.token_storage')->getToken()->getUser();
        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($currentUser);
        $em->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/profil/{id}", name="publicProfile")
     */
    public function publicProfile(User $user, Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $userRepository = $em->getRepository(User::class);
        $commentRepository = $em->getRepository(Commentaire::class);

        $user = $userRepository->find($user->getId());

        $threeLastComments = $commentRepository->getThreeLast($user);

        return $this->render('user/profilPublic.html.twig', [
            'user' => $user,
            'threeLastComments' => $threeLastComments
        ]);
    }
}