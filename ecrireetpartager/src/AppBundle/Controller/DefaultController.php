<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Nouvelle;
use AppBundle\Entity\Categorie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function home(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $dql = "SELECT i FROM AppBundle:Nouvelle i";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $nouvellePublished = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );
        $repository = $em->getRepository(Categorie::class);
        $categories = $repository->findAll();


        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'nouvellePublished' => $nouvellePublished,
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/filter",name="filter")
     */
    public function homeCategorie(Request $request)
    {
        $categories = $request->query->get("categories");
        $em = $this->get('doctrine.orm.entity_manager');
            $dql = "SELECT i FROM AppBundle:Nouvelle i WHERE i.categorie IN :categories ORDER BY i.published_at DESC";
            $query = $em->createQuery($dql)
                ->setParameter('categories', $categories);

        $paginator = $this->get('knp_paginator');
        $nouvellePublished = $paginator->paginate(
            $query,
            $request->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return $this->render('default/index.html.twig', [
            'nouvellePublished' => $nouvellePublished,
            'categorie' => $categorie
        ]);

    }
}
